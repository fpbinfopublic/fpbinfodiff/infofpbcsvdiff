#!/bin/bash -n
# fpbinfocsvdiff.sh , write only what changed in one file.
# 20191203 created Frank Peter Brueckmann, fpb-info.de
#
######################################################

## you can used this for testing this script ##
# echo "Linux;Unix;300;souce;target" > a1.csv
# echo "Linux;Unix;300;souce;target" > a2.csv
# echo "Linux2;Unix;300;souce;target1" >> a1.csv
# echo "Linux2;Unix;300;souce;target2" >> a2.csv
# echo "Linux3;Unix;300;souce;target"  >> a1.csv
# echo "Linux3;Unix;300;souce;Target"  >> a2.csv

##############   CONFIG  ###############################
#for start001(search for start001, you will see why), first oldest, then next oldest
fileAnpassenold=`ls a[0-9].csv | sort -u | head -1`
fileAnpassennew=`ls a[0-9].csv | sort -u | head -2 | tail -1`
#start002, if you like, more , example for b0.csv, b1.csv
fileAnpassenAnpassenold=`ls b[0-9].csv | sort -u | tail -2 | head -1`
fileAnpassenAnpassennew=`ls b[0-9].csv | sort -u | tail -1`


#Diff Filename
# search for diffFile , then you should known why.

# what doing with file what was checked
# a rename , b delete file , c or all other nothing doing for demo 
aldfilesoll="c"


# delemiter in csv file ${Trennzeichen}
Trennzeichen=";"

#goto start* for other config

###################  Check ############################
# check if file exist
Dateiexist(){
# infile="$fileAnpassenold"
# fromfile="$fileAnpassennew"
# diffFile="all-Anpassen.diff"
ausfuehren="ja"
if [ -f "$infile" ] ; then
	echo "found \"$infile\""
else
	echo "File not found: $infile "
	ausfuehren="nein"
fi
if [ -f "$fromfile" ] ; then
	echo "found \"$fromfile\""
else
	echo "File not found: \"$fromfile\" "
	ausfuehren="nein"
fi
if [ -f "$diffFile" ] ; then
	echo "found \"$diffFile\""
else
	echo "File not found: \"$diffFile\" "
	echo "$firstlinedescription" > $diffFile
fi
}

BearbeitetesFile(){
if [[ $aldfilesoll == "a" ]]; then
	mv $infile done.${infile}
	echo "$infile done.${infile}"
elif [[ $aldfilesoll == "b" ]]; then
	rm $infile
	echo "rm $infile"
else
	echo "Demo Mode, no Change old file $infile"
fi 
}
DemoMode(){
if [[ $aldfilesoll == "a" ]]; then
	echo "I will start now with rename old file ${infile} at the end"
	diffFileused=$diffFile
elif [[ $aldfilesoll == "b" ]]; then
	echo "I will start now with delete old file ${infile} at the end"
	diffFileused=$diffFile
else
	echo "Demo Mode, no Change old file $infile and diff file end with demo: $diffFileused."
	diffFileused=${diffFile}.demo
	cp -p $diffFileused ${diffFileused}.bak
	rm $diffFileused
fi	
}

Werte() {
Wert01=`echo $line | cut -d"${Trennzeichen}" -f1`
Wert02=`echo $line | cut -d"${Trennzeichen}" -f2`
Wert03=`echo $line | cut -d"${Trennzeichen}" -f3`
Wert04=`echo $line | cut -d"${Trennzeichen}" -f4`
Wert05=`echo $line | cut -d"${Trennzeichen}" -f5`
Wert06=`echo $line | cut -d"${Trennzeichen}" -f6`
Wert07=`echo $line | cut -d"${Trennzeichen}" -f7`
Wert08=`echo $line | cut -d"${Trennzeichen}" -f8`
Wert09=`echo $line | cut -d"${Trennzeichen}" -f9`
Wert10=`echo $line | cut -d"${Trennzeichen}" -f10`
Wert11=`echo $line | cut -d"${Trennzeichen}" -f11`
Wert12=`echo $line | cut -d"${Trennzeichen}" -f12`
}
Wertegefunden() {
Wertgefunden01=`echo $suchenach | cut -d"${Trennzeichen}" -f1`
Wertgefunden02=`echo $suchenach | cut -d"${Trennzeichen}" -f2`
Wertgefunden03=`echo $suchenach | cut -d"${Trennzeichen}" -f3`
Wertgefunden04=`echo $suchenach | cut -d"${Trennzeichen}" -f4`
Wertgefunden05=`echo $suchenach | cut -d"${Trennzeichen}" -f5`
Wertgefunden06=`echo $suchenach | cut -d"${Trennzeichen}" -f6`
Wertgefunden07=`echo $suchenach | cut -d"${Trennzeichen}" -f7`
Wertgefunden08=`echo $suchenach | cut -d"${Trennzeichen}" -f8`
Wertgefunden09=`echo $suchenach | cut -d"${Trennzeichen}" -f9`
Wertgefunden10=`echo $suchenach | cut -d"${Trennzeichen}" -f10`
Wertgefunden11=`echo $suchenach | cut -d"${Trennzeichen}" -f11`
Wertgefunden12=`echo $suchenach | cut -d"${Trennzeichen}" -f12`
}
WerteVergleich() {
geandert="nein"
if [[ "$Wert01" != "$Wertgefunden01" ]]; then  geandert="ja" ; changedWertgefunden01="$Wertgefunden01" ; else  changedWertgefunden01="" ; fi
if [[ "$Wert02" != "$Wertgefunden02" ]]; then  geandert="ja" ; changedWertgefunden02="$Wertgefunden02" ; else  changedWertgefunden02="" ; fi
if [[ "$Wert03" != "$Wertgefunden03" ]]; then  geandert="ja" ; changedWertgefunden03="$Wertgefunden03" ; else  changedWertgefunden03="" ; fi
if [[ "$Wert04" != "$Wertgefunden04" ]]; then  geandert="ja" ; changedWertgefunden04="$Wertgefunden04" ; else  changedWertgefunden04="" ; fi
if [[ "$Wert05" != "$Wertgefunden05" ]]; then  geandert="ja" ; changedWertgefunden05="$Wertgefunden05" ; else  changedWertgefunden05="" ; fi
if [[ "$Wert06" != "$Wertgefunden06" ]]; then  geandert="ja" ; changedWertgefunden06="$Wertgefunden06" ; else  changedWertgefunden06="" ; fi
if [[ "$Wert07" != "$Wertgefunden07" ]]; then  geandert="ja" ; changedWertgefunden07="$Wertgefunden07" ; else  changedWertgefunden07="" ; fi
if [[ "$Wert08" != "$Wertgefunden08" ]]; then  geandert="ja" ; changedWertgefunden08="$Wertgefunden08" ; else  changedWertgefunden08="" ; fi
if [[ "$Wert09" != "$Wertgefunden09" ]]; then  geandert="ja" ; changedWertgefunden09="$Wertgefunden09" ; else  changedWertgefunden09="" ; fi
if [[ "$Wert10" != "$Wertgefunden10" ]]; then  geandert="ja" ; changedWertgefunden10="$Wertgefunden10" ; else  changedWertgefunden10="" ; fi
if [[ "$Wert11" != "$Wertgefunden11" ]]; then  geandert="ja" ; changedWertgefunden11="$Wertgefunden11" ; else  changedWertgefunden11="" ; fi
if [[ "$Wert12" != "$Wertgefunden12" ]]; then  geandert="ja" ; changedWertgefunden12="$Wertgefunden12" ; else  changedWertgefunden12="" ; fi
#if [[ hallo != hallo ]]; then ; echo "ungleich" ; else  echo "gleich" ; fi
}

Werteclean() {
Wert01=""
Wert02="" 
Wert03=""
Wert04=""
Wert05=""
Wert06=""
Wert07=""
Wert08=""
Wert09=""
Wert10=""
Wert11=""
Wert12=""
Wertgefunden01=""
Wertgefunden02=""
Wertgefunden03=""
Wertgefunden04=""
Wertgefunden05=""
Wertgefunden06=""
Wertgefunden07=""
Wertgefunden08=""
Wertgefunden09=""
Wertgefunden10=""
Wertgefunden11=""
Wertgefunden12=""
}

#start001, copy start until end for other files and put this under the end and mofify this.
infile="$fileAnpassenold"
fromfile="$fileAnpassennew"
diffFile="all-Anpassen.diff"
firstlinedescription="a;b;c;d;e;f;g;h;i;j;k;l;m;n"
Dateiexist #ausfuehren="ja"
if [[ $ausfuehren == "ja" ]]; then
	DemoMode
	Werteclean
	#for
	echo "Check: from $fromfile in $infile and the diff will write in $diffFile"
	while read line ; do
	echo "input file line: $line"
	echo "$line"
	Werte
	#modify this, should be same how by grep in suchenach behind the else
	if [ -z $Wert01 ] || [ -z $Wert02 ] || [ -z $Wert03 ]; then
		echo "some empty: grep $Wert01 $infile | grep $Wert02 | grep $Wert03"
	# Not old to check?
	# modify elif
	# elif [[ $Wert01 == "ignore"  ]]; then
		# echo "will be ignored: $Wert01"
		
	### if all ok, then I start.
	else
		#modify this that can finde the line for diff.
		suchenach=`grep "$Wert01;" $infile | grep ";$Wert02;" | grep ";$Wert03;" `
		echo "suchenach: $suchenach"
		Wertegefunden
		WerteVergleich
		if [[ $geandert == "ja" ]]; then
			echo "$fromfile;$line;$infile: \"$changedWertgefunden01\" ,\"$changedWertgefunden02\" ,\"$changedWertgefunden03\" ,\"$changedWertgefunden04\" ,\"$changedWertgefunden05\" ,\"$changedWertgefunden06\" ,\"$changedWertgefunden07\" ,\"$changedWertgefunden08\" ,\"$changedWertgefunden09\" ,\"$changedWertgefunden10\" ,\"$changedWertgefunden11\" ,\"$changedWertgefunden12\""| tee -a $diffFileused
		else
			echo "keine Aenderung"
		fi
	fi
	Werteclean
	done < $fromfile
	BearbeitetesFile  # delete or mv oldes file or nothig for test script. 
fi
#End001, copy start until end for other files and put this under the end and mofify this.



